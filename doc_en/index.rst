.. 跨領域區域研究資料集 documentation master file, created by
   sphinx-quickstart on Tue Nov 21 14:06:57 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to 跨領域區域研究資料集's documentation!
================================================

.. toctree::
   :maxdepth: 2

   user-guide

.. seealso::
   :doc:`contents`

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
