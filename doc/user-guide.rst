==========
操作手冊
==========

本操作手冊說明如何使用CKAN介面管理、發布及搜尋資料。 CKAN同時提供功能豐富的API介面，
讓開發者更易於自行擴充功能或是連結至其他資訊系統。相關API資訊可參考 `官方API文件 <http://docs.ckan.org
/en/latest/api/index.html>`_ 。


部分CKAN使用者介面功能僅限於系統管理員操作，相關說明請參考 `官方系統管理者文件 <http://docs.ckan.org/e
n/latest/sysadmin-guide.html>`_ 。
